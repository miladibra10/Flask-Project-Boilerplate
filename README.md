# Flask-Project-Boilerplate
a bioler plate project developed with flask, Nginx, Docker, And PostgreSQL.

# Deploy
for deploying this Project you should have `docker-compose` and `docker` installed in your system. use command below to deploy Project:
```
docker-compose up -d --build
```

after running this command you should migrate Database migrations to PostgreSQL with this command:

```
docker-compose run app python manage.py db init
docker-compose run app python manage.py db migrate
docker-compose run app python manage.py db upgrade
```

after running these commands server is deployed on `http://localhost:8080` url.

# Swagger Documentation
after deploying Project, Swagger Documentation is available in `http://localhost:8080/docs.html` url.
