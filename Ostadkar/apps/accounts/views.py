from flask_restful import Resource, request
from flask_restful import marshal_with, marshal
from .serializers import user_fields, user_list_fields, user_post_parser, login_parser
from .models import User, BlackListedToken
from db import db
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt
from flask_restful_swagger import swagger


class UserResource(Resource):
    @swagger.operation(
        notes='Get all users or one specific user',
        parameters=[
            {
                "name": "user_id",
                "description": "id for specific user",
                "required": False,
                "allowMultiple": False,
                "dataType": 'int',
                "paramType": "id"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "All users retrieved successfully"
            },
            {
                "code": 200,
                "message": "Specific user retrieved successfully"
            }
        ]
    )
    def get(self, user_id=None):
        if user_id:
            user = User.query.filter_by(id=user_id).first()
            return marshal(user, user_fields)
        else:
            args = request.args.to_dict()
            limit = args.get('limit', 0)
            offset = args.get('offset', 0)

            args.pop('limit', None)
            args.pop('offset', None)

            users = User.query.filter_by(**args).order_by(User.id)
            if limit:
                users = users.limit(limit)

            if offset:
                users = users.offset(offset)

            users = users.all()

            return marshal({
                'count': len(users),
                'users': [marshal(u, user_fields) for u in users]
            }, user_list_fields), 200

    @swagger.operation(
        notes='Register as a user',
        parameters=[
            {
                "name": "body",
                "description": "a user object",
                "required": True,
                "allowMultiple": False,
                "dataType": 'user',
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created."
            },
            {
                "code": 400,
                "message": "Invalid input"
            }
        ]
    )
    @marshal_with(user_fields)
    def post(self):
        args = user_post_parser.parse_args()

        user = User(**args)
        user.password = User.generate_hash(args.get("password"))
        db.session.add(user)
        db.session.commit()

        return user, 201

    @swagger.operation(
        notes='update a user',
        parameters=[
            {
                "name": "body",
                "description": "a user object",
                "required": True,
                "allowMultiple": False,
                "dataType": 'user',
                "paramType": "body"
            },
            {
                "name": "user_id",
                "description": "id for specific user",
                "required": True,
                "allowMultiple": False,
                "dataType": 'int',
                "paramType": "id"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Edited."
            },
            {
                "code": 400,
                "message": "Invalid input"
            },
            {
                "code": 404,
                "message": "Not Found"
            }
        ]
    )
    @marshal_with(user_fields)
    @jwt_required
    def put(self, user_id=None):
        jwt_user = get_jwt_identity()
        user = User.query.get(user_id)

        json_data = request.get_json(force=True)

        if not json_data:
            return {'message': 'No input data provided'}, 400

        if jwt_user != user.phone_number:
            return {'message': 'Wrong credentials'}, 400

        if 'first_name' in json_data:
            user.first_name = json_data['first_name']
        if 'last_name' in json_data:
            user.last_name = json_data['last_name']
        if 'phone_number' in json_data:
            user.phone_number = json_data['phone_number']

        db.session.commit()
        return user, 200

    @swagger.operation(
        notes='Delete a user',
        parameters=[
            {
                "name": "user_id",
                "description": "id for specific user",
                "required": True,
                "allowMultiple": False,
                "dataType": 'int',
                "paramType": "id"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Deleted."
            },
            {
                "code": 404,
                "message": "Not Found"
            }
        ]
    )
    @marshal_with(user_fields)
    @jwt_required
    def delete(self, user_id=None):
        jwt_user = get_jwt_identity()
        user = User.query.get(user_id)

        if jwt_user != user.phone_number:
            return {'message': 'Wrong credentials'}, 400

        db.session.delete(user)
        db.session.commit()

        return user, 200


class UserLogin(Resource):
    @swagger.operation(
        notes='login',
        parameters=[
            {
                "name": "body",
                "description": "phone number and password",
                "required": True,
                "allowMultiple": False,
                "dataType": 'login json',
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "Json tokens"
            },
            {
                "code": 400,
                "message": "Wrong Credentials"
            },
            {
                "code": 404,
                "message": "User Not Found"
            }
        ]
    )
    def post(self):
        data = login_parser.parse_args()
        user = User.find_by_phone_number(data['phone_number'])

        if not user:
            return {'message': 'No user with {} phone_number exist'.format(data['phone_number'])}, 404

        if User.verify_hash(data['password'], user.password):
            access_token = create_access_token(identity=data['phone_number'])
            refresh_token = create_refresh_token(identity=data['phone_number'])
            return {
                'message': 'Logged in as {}'.format(user.phone_number),
                'access_token': access_token,
                'refresh_token': refresh_token
            }, 200
        else:
            return {'message': 'Wrong credentials'}, 400


class UserLogout(Resource):
    @swagger.operation(
        notes='logout, Access token should be in header to be Logged out',
        responseMessages=[
            {
                "code": 200,
                "message": "Access token has been revoked"
            },
            {
                "code": 500,
                "message": "Internal Server Error"
            }
        ]
    )
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = BlackListedToken(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}, 200
        except Exception as e:
            return {'message': 'Something went wrong'}, 500


class RefreshToken(Resource):
    @swagger.operation(
        notes='refresh token, Refresh token should be in header to generate another access token',
        responseMessages=[
            {
                "code": 200,
                "message": "Access token has been revoked"
            },
            {
                "code": 500,
                "message": "Internal Server Error"
            }
        ]
    )
    @jwt_refresh_token_required
    def post(self):
        user = get_jwt_identity()
        access_token = create_access_token(identity=user)
        return {'access_token': access_token}, 200
