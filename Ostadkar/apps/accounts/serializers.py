from flask_restful import reqparse
from flask_restful import fields


user_fields = {
    'id': fields.Integer,
    'first_name': fields.String,
    'last_name': fields.String,
    'phone_number': fields.String,
    'registration_date': fields.String,
}

user_list_fields = {
    'count': fields.Integer,
    'users': fields.List(fields.Nested(user_fields)),
}

user_post_parser = reqparse.RequestParser()
user_post_parser.add_argument('first_name', type=str, required=True, location=['json'],
                              help='first_name parameter is required')
user_post_parser.add_argument('last_name', type=str, required=True, location=['json'],
                              help='last_name parameter is required')
user_post_parser.add_argument('phone_number', type=str, required=True, location=['json'],
                              help='phone_number parameter is required')
user_post_parser.add_argument('password', type=str, required=True, location=['json'],
                              help='password parameter is required')


login_parser = reqparse.RequestParser()
login_parser.add_argument('phone_number', type=str, required=True, location=['json'], help='phone_number is required')
login_parser.add_argument('password', type=str, required=True, location=['json'], help='password is required')


