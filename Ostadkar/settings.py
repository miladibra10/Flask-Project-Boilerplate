import os


class Config:
    SQLALCHEMY_DATABASE_URI = None
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BUNDLE_ERRORS = True
    SECRET_KEY = None
    DEBUG = True
    PORT = None
    HOST = None


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///flask.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BUNDLE_ERRORS = True
    SECRET_KEY = "3j4k5h43kj5hj234b5jh34bk25b5k234j5bk2j3b532"
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 8080


class ProductionConfig(Config):
    def __init__(self):
        self.url = os.environ["POSTGRES_URL"]
        self.user = os.environ["POSTGRES_USER"]
        self.password = os.environ["POSTGRES_PASSWORD"]
        self.db = os.environ["POSTGRES_DB"]
        self.secret = os.environ["APP_SECRET_KEY"]
        self.SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{pw}@{url}/{db}'.format(user=self.user, pw=self.password, url=self.url, db=self.db)
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False
        self.BUNDLE_ERRORS = True
        self.SECRET_KEY = self.secret
        self.DEBUG = False


settings = ProductionConfig()
