from flask import Flask
from flask.json import jsonify
from flask_restful import Api
from werkzeug.exceptions import default_exceptions, HTTPException
from flask_jwt_extended import JWTManager
from flask_restful_swagger import swagger
from settings import settings


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
app.config['BUNDLE_ERRORS'] = settings.BUNDLE_ERRORS
app.config['JWT_SECRET_KEY'] = settings.SECRET_KEY
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

jwt = JWTManager(app)

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    from apps.accounts.models import BlackListedToken
    jti = decrypted_token['jti']
    return BlackListedToken.is_jti_blacklisted(jti)


@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code
    return jsonify(error=str(e)), code


for e in default_exceptions:
    app.register_error_handler(e, handle_error)


api = swagger.docs(Api(app), apiVersion='1.0', api_spec_url='/docs')
# api.prefix = '/api'



from apps.accounts.views import UserResource, UserLogin, UserLogout, RefreshToken

api.add_resource(UserResource, '/users/', '/users/<int:user_id>/')
api.add_resource(UserLogin, '/token/obtain/')
api.add_resource(RefreshToken, '/token/refresh/')
api.add_resource(UserLogout, '/logout/')

if __name__ == '__main__':
    # app.run(host=settings.HOST, port=settings.PORT, debug=settings.DEBUG)
    app.run()
